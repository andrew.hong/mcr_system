<?php
if (php_sapi_name() !='cli') die("Access denied.");

include 'config.php';
$interest_rate = INTEREST_RATE_PER_ANNUM;
$daily_interest_rate = (INTEREST_RATE_PER_ANNUM / 100) / 365;
$systemUser = "Interest Payments (" . $interest_rate . "% p/a)";
echo "Daily interest rate: " . $daily_interest_rate . "%\n\n";

try {
	$conn = new PDO("mysql:host=localhost;dbname=" . MYSQL_DATABASE, MYSQL_USERNAME, MYSQL_PASSWORD);
	$userQuery = $conn->prepare("SELECT * FROM userdata");
	$userQuery->execute();
	foreach ($userQuery->fetchAll() as $user) {
		echo "Closing balance for " . $user["username"] . ": $" . number_format($user["balance"]) . "\n";
		echo "Interest earned: $" . number_format($daily_interest_rate * $user["balance"], 2, ".", "") . "\n"; 
		$final_balance = number_format($user["balance"] + $daily_interest_rate * $user["balance"], 2, ".", "");
		echo "Final balance: $" . $final_balance . "\n";
		$updateBalance = $conn->prepare("UPDATE userdata SET balance = ? WHERE username = ?");
		$updateBalance->bindParam(1, $final_balance);
		$updateBalance->bindParam(2, $user["username"]);
		$updateBalance->execute();
		$addTransaction = $conn->prepare("INSERT INTO transactiondata (sender, receiver, amount) VALUES (?, ?, ?)");
		$addTransaction->bindParam(1, $systemUser);
		$addTransaction->bindParam(2, $user["username"]);
		$interest_earned = number_format($daily_interest_rate * $user["balance"], 2, ".", "");
		$addTransaction->bindParam(3, $interest_earned);
		$addTransaction->execute();
		echo "Balance updated for user.\n\n";
	}
} catch (Exception $ex) {
	echo "Error: " . $ex;
}
?>

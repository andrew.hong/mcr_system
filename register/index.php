<?php 
	require '../config.php';
	require '../header.php'; 
	$error = $_GET["error"];
	$error_array = array("One or more fields are empty.", "Invalid username.", "Password must be at least 8 characters.", "Email address is not valid.", "Username is taken.", "Email is taken.", "Server error.");
	if (is_numeric($error) && $error <= 7) $msg = '<div class="alert alert-danger">' . $error_array[$error - 1] . '</div>';
	if ($_GET["success"]) $msg = '<div class="alert alert-success">Registration successful. Please wait for a confirmation email.</div>';
?>
<body>
	<br />
	<br />
	<div class="jumbotron">
		<div class="container">
			<h1>Registration <span style="font-size: 12px">Already signed up? <a href="/login">Log in</a>.</span></h1>
			<p>You will need your username, a password and an email address. Please note that your username should match your in-game username, not nickname.</p>
			<br />
			<?php if (isset($msg)) echo $msg; ?>
			<form action="create.php" method="POST">
				<label for="username">Username</label>
				<input type="text" name="username" class="form-control" placeholder="In-game username..." />
				<br />
				<label for="password">Password (must be at least 8 characters, can contain special characters)</label>
				<input type="password" name="password" class="form-control" placeholder="Password..." />
				<br />
				<label for="email">Email address</label>
				<input type="email" name="email" class="form-control" placeholder="Email address..." />
				<br />
				<span style="float: right">
					<input type="submit" class="btn btn-success" value="Process registration"/>
				</span>
			</form>
		</div>
	</div>
</body>

<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../vendor/autoload.php';
require '../config.php';

$username = $_POST["username"];
$password = $_POST["password"];
$email = $_POST["email"];

// Perform validations
// Error guide:
// 1: Empty username, password or email
// 2: Invalid username found
// 3: Password too short
// 4: Invalid email
// 5: Username taken.
// 6: Email taken.
// 7: Server error.

if (empty($username) || empty($password) || empty($email)) {
	header("Location: /register/?error=1");
	die("One or more fields were empty.");
}

if(!ctype_alnum(str_replace(array('-', '_'), '', $username))) { 
	header("Location: /register/?error=2");
	die("Username must only contain letters, numbers, dashes and underscores.");
}

if (strlen($password) < 8) {
	header("Location: /register/?error=3");
	die("Password is too short.");
}

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	header('Location: /register/?error=4');
	die("Email is invalid.");
}

// Input validation complete.
// Now perform database checks.

try {
	$conn = new PDO("mysql:host=localhost;dbname=" . MYSQL_DATABASE, MYSQL_USERNAME, MYSQL_PASSWORD);
	$userCheck = $conn->prepare('SELECT id FROM userdata WHERE username = ?');
	$userCheck->bindParam(1, $username);
	$userCheck->execute();
	if ($userCheck->rowCount() > 0) {
		header("Location: /register/?error=5");
		die("Username already taken.");
	}
	$emailCheck = $conn->prepare('SELECT id FROM userdata WHERE email = ?');
	$emailCheck->bindParam(1, $email);
	$emailCheck->execute();
	if ($emailCheck->rowCount() > 0) {
		header("Location: /register/?error=6");
		die("Email already taken.");
	}
	$insertUser = $conn->prepare('INSERT INTO userdata (username, password, email, confirmation, balance) VALUES (?, ?, ?, ?, 0)');

	$hashed_password = hash("sha256", $password);
	$insertUser->bindParam(1, $username);
	$insertUser->bindParam(2, $hashed_password);
	$insertUser->bindParam(3, $email);
	$confirmation_token = bin2hex(random_bytes(16));
	$insertUser->bindParam(4, $confirmation_token);
	$insertUser->execute();

	$mail = new PHPMailer(true);
	$mail->SMTPDebug = 2;
	$mail->isSMTP();
	$mail->Host = SMTP_HOST;
	$mail->SMTPAuth = true;
	$mail->Username = SMTP_USERNAME;
	$mail->Password = SMTP_PASSWORD;
	$mail->SMTPSecure = 'tls';
	$mail->Port = 587;
	$mail->setFrom(SMTP_FROM, 'System');
	$mail->addAddress($email, 'User');
	$mail->isHTML(true);
	$mail->Subject = 'Confirm your MCR registration';
	$mail->Body = '<h3>Confirm registration</h3><br /><p>Click <a href="' . GENERAL_URL . '/register/confirm.php?token=' . $confirmation_token . '">here</a> to finish registration.</p><p>If you did not sign up for this service, please disregard the contents of this email and delete it.</p>';
	$mail->send();
	header('Location: /register/?success=true');
} catch (Exception $ex) {
	header('Location: /register/?error=7');
	die("Server error.");
}
?>
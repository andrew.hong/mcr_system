<?php
require '../vendor/autoload.php';
require '../config.php';

if (isset($_GET["token"]) && !empty($_GET["token"]) && ctype_alnum($_GET["token"]) && strlen($_GET["token"]) == 32) {
	$token = $_GET["token"];
	try {
		$conn = new PDO("mysql:host=localhost;dbname=" . MYSQL_DATABASE, MYSQL_USERNAME, MYSQL_PASSWORD);
		$checkToken = $conn->prepare("SELECT username FROM userdata WHERE confirmation = ?");
		$checkToken->bindParam(1, $token);
		$checkToken->execute();
		if ($checkToken->rowCount() > 0) {
			$clearToken = $conn->prepare("UPDATE userdata SET confirmation = '' WHERE confirmation = ?");
			$clearToken->bindParam(1, $token);
			$clearToken->execute();
		}
	} catch (Exception $ex) {
		// Unhandled error. :P
	}
}

header("Location: /login");
?>

<?php
session_start();
require '../../config.php';
require '../auth.php';

// Perform validations
// Error guide:
// 1: Voucher code not specified or is not formatted properly.
// 2: Voucher code invalid.
// 3: Server error.

if (empty($_POST["voucher"]) || strlen($_POST["voucher"]) !== 11) {
	header("Location: /home/redeem/?error=1");
	die("Voucher code not specified or is not formatted properly.");
}

try {
	$conn = new PDO("mysql:host=localhost;dbname=" . MYSQL_DATABASE, MYSQL_USERNAME, MYSQL_PASSWORD);
	$checkVoucher = $conn->prepare("SELECT id, value FROM voucherdata WHERE voucher = ? AND redeemed = 0");
	$checkVoucher->bindParam(1, $_POST["voucher"]);
	$checkVoucher->execute();
	if ($checkVoucher->rowCount() == 0) {
		header("Location: /home/redeem/?error=2");
		die("Voucher code invalid.");
	}
	$systemUsername = "System";
	$voucherData = $checkVoucher->fetch();
	$addTransaction = $conn->prepare("INSERT INTO transactiondata (sender, receiver, amount) VALUES (?, ?, ?)");
	$addTransaction->bindParam(1, $systemUsername);
	$addTransaction->bindParam(2, $_SESSION["user"]);
	$addTransaction->bindParam(3, $voucherData["value"]);
	$addTransaction->execute();
	$newBalance = $_SESSION["balance"] + $voucherData["value"];
	$balanceUpdate = $conn->prepare("UPDATE userdata SET balance = ? WHERE username = ?");
	$balanceUpdate->bindParam(1, $newBalance);
	$balanceUpdate->bindParam(2, $_SESSION["user"]);
	$balanceUpdate->execute();
	$voucherUpdate = $conn->prepare("UPDATE voucherdata SET redeemed = 1 WHERE voucher = ?");
	$voucherUpdate->bindParam(1, $_POST["voucher"]);
	$voucherUpdate->execute();
	header("Location: /home/redeem/?success=" . $voucherData["value"]);
} catch (Exception $ex) {
	header('Location: /home/redeem/?error=3');
	die("Server error.");
}

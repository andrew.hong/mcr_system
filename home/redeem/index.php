<?php 
	require '../../config.php'; 
	require '../auth.php'; 
	require '../../header.php'; 
	$user = $_SESSION["user"];
	$error = $_GET["error"];
	$error_array = array("One or more fields are empty.", "Voucher code invalid.", "Server error.");
	if (is_numeric($error) && $error <= 7) $msg = '<div class="alert alert-danger">' . $error_array[$error - 1] . '</div>';
?>
<body>
	<br />
	<div class="jumbotron">
		<div class="container">
			<h1>Redeem voucher</h1>
			<p>Please enter the voucher code you received in-game.</p>
			<br />
			<?php if (isset($msg)) echo $msg; ?>
			<?php if (isset($_GET["success"])) echo '<div class="alert alert-success">Voucher redeemed for $' . number_format($_GET["success"]) . '.</div>'; ?>
			<form action="redeem.php" method="POST">
				<label for="amount">Amount</label>
				<input type="text" name="voucher" class="form-control" placeholder="xxx-xxx-xxx" required />
				<br />
				<span style="float: right">
					<input type="submit" class="btn btn-success" value="Redeem" />
				</span>
			</form>
		</div>
	</div>
</body>

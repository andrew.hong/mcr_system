<?php 
	require '../../config.php'; 
	require '../auth.php'; 
	require '../../header.php'; 
	$user = $_SESSION["user"];
	$transactionData = "";
	try {
		$conn = new PDO("mysql:host=localhost;dbname=" . MYSQL_DATABASE, MYSQL_USERNAME, MYSQL_PASSWORD);
		$transactionData = $conn->prepare("SELECT * FROM transactiondata WHERE sender = :user OR receiver = :user ORDER BY transactiondate DESC
 LIMIT 10");
		$transactionData->bindParam(":user", $user);
		$transactionData->execute();
	} catch (Exception $ex) {
		header("Location: /home/logout");
		die("System error. Please try again later.");
	}
?>
<body>
	<br />
	<div class="jumbotron">
		<div class="container">
			<h1>Last 10 transactions</h1>
			<p>If you require more information, please contact us.</p>
			<br />
		</div>
	</div>
	<br />
	<div class="jumbotron">
		<div class="container">
			<?php 
				if ($transactionData->rowCount() == 0) { 
					echo '<div class="alert alert-primary">No transactions found.</div>'; 
				} else {
					$transactions = $transactionData->fetchAll();
					echo '<div class="table-responsive">';
					echo '<table class="table table-dark table-striped table-bordered table-hover">';
					echo '<thead>';
					echo '<tr>';
					echo '<th scope="col">Transaction ID</th>';
					echo '<th scope="col">Sender</th>';
					echo '<th scope="col">Receiver</th>';
					echo '<th scope="col">Amount</th>';
					echo '<th scope="col">Date</th>';
					echo '</tr>';
					echo '</thead>';
					echo '<tbody>';
					foreach ($transactions as $transaction) {
						echo '<tr>';
						echo '<td>' . $transaction["uniqueid"] . '</td>';
						echo '<td>' . $transaction["sender"] . '</td>';
						echo '<td>' . $transaction["receiver"] . '</td>';
						if ($transaction["receiver"] == $user) {
							echo '<td>+$' . number_format($transaction["amount"], 2, '.', '') . '</td>';
						} else {
							echo '<td>-$' . number_format($transaction["amount"], 2, '.', '') . '</td>';
						}
						echo '<td>' . $transaction["transactiondate"] . '</td>';
						echo '</tr>';
					}
					echo '</tbody>';
					echo '</table>';
					echo '</div>';
				}
			?>
		</div>
	</div>
</body>
<?php
session_start();
require '../../config.php';
require '../auth.php';

$recepient = $_POST["recepient"];
$amount = number_format($_POST["amount"]);

// Perform validations
// Error guide:
// 1: Password empty
// 2: Password does not meet complexity requirements.
// 3: Server error

if (empty($_POST["password"])) {
	header("Location: /home/account/?error=1");
	die("Password empty.");
}

if (strlen($_POST["password"]) < 8) {
	header("Location: /home/account/?error=2");
	die("Password does not meet complexity requirements.");
}

// Input validation complete.
// Now perform database checks.

try {
	$hashed_password = hash("sha256", $_POST["password"]);
	$conn = new PDO("mysql:host=localhost;dbname=" . MYSQL_DATABASE, MYSQL_USERNAME, MYSQL_PASSWORD);
	$updatePassword = $conn->prepare("UPDATE userdata SET password = ? WHERE username = ?");
	$updatePassword->bindParam(1, $hashed_password);
	$updatePassword->bindParam(2, $_SESSION["user"]);
	$updatePassword->execute();
	header("Location: /home/account/?success=password_updated");
} catch (Exception $ex) {
	header("Location: /home/account/?error=3");
	die("Server error.");
}
?>
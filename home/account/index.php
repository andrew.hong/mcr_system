<?php 
	require '../../config.php'; 
	require '../auth.php'; 
	require '../../header.php'; 
	$userdata = "";
	try {
		$conn = new PDO("mysql:host=localhost;dbname=" . MYSQL_DATABASE, MYSQL_USERNAME, MYSQL_PASSWORD);
		$userquery = $conn->prepare("SELECT * FROM userdata WHERE username = ?");
		$userquery->bindParam(1, $_SESSION["user"]);
		$userquery->execute();
		$userdata = $userquery->fetch();
	} catch (Exception $ex) {
		header("Location: /home/logout");
		die("System error.");
	}
?>
<body>
	<br />
	<div class="jumbotron">
		<div class="container">
			<h1>Account Management</h1>
			<p>Change your password, or contact support.</p>
			<br />
			<?php if ($_GET["success"] == "password_updated") echo '<div class="alert alert-success">Password updated.</div>'; ?>
			<p><b>Email address: </b><?php echo $userdata["email"]; ?></p>
			<p><b>Support email: </b> andrew@andrew-hong.me</p>
			<form action="update.php" method="POST">
				<label for="password">New password (must be at least 8 characters)</label>
				<input type="password" name="password" class="form-control" placeholder="Your new password..." required />
				<br />
				<span style="float: right">
					<input type="submit" class="btn btn-success" value="Change password" />
				</span>
			</form>
		</div>
	</div>
</body>

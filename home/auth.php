<?php
session_start();
if (empty($_SESSION["user"])) {
	header("Location: /login");
	die("Not logged in.");
}
try {
	$conn = new PDO("mysql:host=localhost;dbname=" . MYSQL_DATABASE, MYSQL_USERNAME, MYSQL_PASSWORD);
	$query = $conn->prepare("SELECT id, balance FROM userdata WHERE USERNAME = ?");
	$query->bindParam(1, $_SESSION["user"]);
	$query->execute();
	if ($query->rowCount() == 0) {
		header("Location: /login");
		die("Not authenticated.");
	}
	$userData = $query->fetch();
	$_SESSION["balance"] = $userData["balance"];
} catch (Exception $ex) {
	header("Location: /");
	die("Server error.");
}
?>
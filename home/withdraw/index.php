<?php 
	require '../../config.php'; 
	require '../auth.php'; 
	require '../../header.php'; 
	$userdata = "";
	try {
		$conn = new PDO("mysql:host=localhost;dbname=" . MYSQL_DATABASE, MYSQL_USERNAME, MYSQL_PASSWORD);
		$userquery = $conn->prepare("SELECT * FROM userdata WHERE username = ?");
		$userquery->bindParam(1, $_SESSION["user"]);
		$userquery->execute();
		$userdata = $userquery->fetch();
	} catch (Exception $ex) {
		header("Location: /home/logout");
		die("System error.");
	}
	$error = $_GET["error"];
	$error_array = array("One or more fields are empty.", "Invalid amount. You can only send or receive $100.00 to $999999999.", "Insufficient funds.", "Server error.");
	if (is_numeric($error) && $error <= 7) $msg = '<div class="alert alert-danger">' . $error_array[$error - 1] . '</div>';
?>
<body>
	<br />
	<div class="jumbotron">
		<div class="container">
			<h1>Withdraw</h1>
			<p>Enter the amount you wish to withdraw. The request will be processed in 12-24 hours. Note that funds are withdrawn immediately.</p>
			<br />
			<?php if (isset($msg)) echo $msg; ?>
			<?php if ($_GET["success"]) echo '<div class="alert alert-success">Your request was received. Please allow some time for it to be processed.</div>'; ?>
			<form action="withdraw.php" method="POST">
				<label for="amount">Amount to withdraw</label>
				<input type="number" min="0.00" max="99999999.99" step="0.01" name="amount" class="form-control" placeholder="1000.00" required />
				<br />
				<span style="float: right">
					<input type="submit" class="btn btn-success" value="Withdraw" />
				</span>
			</form>
		</div>
	</div>
</body>

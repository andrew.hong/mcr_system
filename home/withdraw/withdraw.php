<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

session_start();

require '../../config.php';
require '../../vendor/autoload.php';
require '../auth.php';

$amount = $_POST["amount"];

// Perform validations
// Error guide:
// 1: One or more fields were empty.
// 2: Invalid amount.
// 3: Insufficient funds.
// 4: Server error

if (empty($amount)) {
	header("Location: /home/withdraw/?error=1");
	die("One or more fields were empty.");
}

if (!is_numeric($amount)) {
	header("Location: /home/withdraw/?error=2");
	die("Invalid amount.");	
}

if ($amount > 99999999 || $amount < 100) {
	header("Location: /home/withdraw/?error=2");
	die("Invalid amount.");
}

if (strlen($amount) > 16) {
	header("Location: /home/withdraw/?error=2");
	die("Invalid amount.");	
}

// Input validation complete.
// Now perform database checks.

try {
	$conn = new PDO("mysql:host=localhost;dbname=" . MYSQL_DATABASE, MYSQL_USERNAME, MYSQL_PASSWORD);
	$balanceCheck = $conn->prepare("SELECT balance FROM userdata WHERE username = ?");
	$balanceCheck->bindParam(1, $_SESSION["user"]);
	$balanceCheck->execute();
	$balanceData = $balanceCheck->fetch();
	if ($balanceData["balance"] < $amount) {
		header("Location: /home/withdraw/?error=3");
		die("Insufficient funds.");
	}
	$newBalance = $balanceData["balance"] - $amount;
	$systemUser = "System";
	$updateBalance = $conn->prepare("UPDATE userdata SET balance = ? WHERE username = ?");
	$updateBalance->bindParam(1, $newBalance);
	$updateBalance->bindParam(2, $_SESSION["user"]);
	$updateBalance->execute();
	$addTransaction = $conn->prepare("INSERT INTO transactiondata (sender, receiver, amount) VALUES (?, ?, ?)");
	$addTransaction->bindParam(1, $_SESSION["user"]);
	$addTransaction->bindParam(2, $systemUser);
	$addTransaction->bindParam(3, $amount);
	$addTransaction->execute();

	// Send notification
	$mail = new PHPMailer(true);
	$mail->SMTPDebug = 0;
	$mail->isSMTP();
	$mail->Host = SMTP_HOST;
	$mail->SMTPAuth = true;
	$mail->Username = SMTP_USERNAME;
	$mail->Password = SMTP_PASSWORD;
	$mail->SMTPSecure = 'tls';
	$mail->Port = 587;
	$mail->setFrom(SMTP_FROM, 'System');
	$mail->addAddress(SYSTEM_EMAIL, 'Administrator');
	$mail->isHTML(true);
	$mail->Subject = 'Withdrawal Request';
	$mail->Body = '<h3>Withdrawal Request Received</h3><br /><p>Recepient: ' . $_SESSION["user"] . '</p><p>Amount owed: ' . $amount . '</p>';
	$mail->send();

	header("Location: /home/withdraw/?success=true");
} catch (Exception $ex) {
	header('Location: /home/withdraw/?error=4');
	die("Server error.");
}
?>
<?php 
	require '../../config.php'; 
	require '../auth.php'; 
	require '../../header.php'; 
	$user = $_SESSION["user"];
	$error = $_GET["error"];
	$error_array = array("One or more fields were empty.", "Invalid amount. You can only send or receive $100.00 to $999999999.", "Recepient does not exist or has not confirmed their account.", "Insufficient funds.", "Server error.");
	if (is_numeric($error) && $error <= 7) $msg = '<div class="alert alert-danger">' . $error_array[$error - 1] . '</div>';
?>
<body>
	<br />
	<div class="jumbotron">
		<div class="container">
			<h1>Send money</h1>
			<p>Please enter the recepient in-game name and the amount.</p>
			<br />
			<?php if (isset($msg)) echo $msg; ?>
			<form action="process.php" method="POST">
				<label for="recepient">Recepient (in-game username)</label>
				<input type="text" name="recepient" class="form-control" placeholder="Recepient..." required />
				<br />
				<label for="amount">Amount</label>
				<input type="number" min="0.00" max="99999999.99" step="0.01" name="amount" class="form-control" placeholder="1000.00" required />
				<br />
				<span style="float: right">
					<input type="submit" class="btn btn-success" value="Send" />
				</span>
			</form>
		</div>
	</div>
</body>

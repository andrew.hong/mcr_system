<?php
session_start();
require '../../config.php';
require '../auth.php';

$recepient = $_POST["recepient"];
$amount = $_POST["amount"];

// Perform validations
// Error guide:
// 1: One or more fields were empty.
// 2: Invalid amount.
// 3: Recepient does not exist or has not confirmed their account.
// 4: Insufficient funds.
// 5: Server error

if (empty($recepient) || empty($amount)) {
	header("Location: /home/send/?error=1");
	die("One or more fields were empty.");
}

if ($amount > 99999999 || $amount < 100) {
	header("Location: /home/send/?error=2");
	die("Invalid amount.");
}

if (trim($recepient) == $_SESSION["user"]) {
	header("Location: /home/send/?error=1");
	die("One or more fields were empty.");
}

// Input validation complete.
// Now perform database checks.

try {
	$conn = new PDO("mysql:host=localhost;dbname=" . MYSQL_DATABASE, MYSQL_USERNAME, MYSQL_PASSWORD);
	$recepientCheck = $conn->prepare("SELECT id, balance FROM userdata WHERE username = ? and confirmation = ''");
	$recepientCheck->bindParam(1, $recepient);
	$recepientCheck->execute();
	if ($recepientCheck->rowCount() == 0) {
		header("Location: /home/send/?error=3");
		die("Recepient does not exist or has not confirmed their account.");
	}
	$recepientData = $recepientCheck->fetch();
	$balanceCheck = $conn->prepare("SELECT balance FROM userdata WHERE username = ?");
	$balanceCheck->bindParam(1, $_SESSION["user"]);
	$balanceCheck->execute();
	$balanceData = $balanceCheck->fetch();
	if ($balanceData["balance"] < $amount) {
		header("Location: /home/send/?error=4");
		die("Insufficient funds.");
	}
	$senderBalance = $balanceData["balance"] - $amount;
	$senderBalanceUpdate = $conn->prepare("UPDATE userdata SET balance = ? WHERE username = ?");
	$senderBalanceUpdate->bindParam(1, $senderBalance);
	$senderBalanceUpdate->bindParam(2, $_SESSION["user"]);
	$senderBalanceUpdate->execute();
	$recepientBalance = $recepientData["balance"] + $amount;
	$recepientBalanceUpdate = $conn->prepare("UPDATE userdata SET balance = ? WHERE username = ?");
	$recepientBalanceUpdate->bindParam(1, $recepientBalance);
	$recepientBalanceUpdate->bindParam(2, $recepient);
	$recepientBalanceUpdate->execute();
	$addTransaction = $conn->prepare("INSERT INTO transactiondata (sender, receiver, amount) VALUES (?, ?, ?)");
	$addTransaction->bindParam(1, $_SESSION["user"]);
	$addTransaction->bindParam(2, $recepient);
	$addTransaction->bindParam(3, $amount);
	$addTransaction->execute();
	header("Location: /home/transactions");
} catch (Exception $ex) {
	header('Location: /home/send/?error=5');
	die("Server error.");
}
?>
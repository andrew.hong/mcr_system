<?php
require '../vendor/autoload.php';
require '../config.php';

$username = $_POST["username"];
$password = $_POST["password"];
$hashed_password = hash("sha256", $password);
$usernameType = "";

// Perform validations
// Error guide:
// 1: Empty username, password or email
// 2: Invalid username
// 3: Invalid credentials
// 4: Account not confirmed
// 5: Server error

if (empty($username) || empty($password)) {
	header("Location: /login/?error=1");
	die("One or more fields were empty.");
}

if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
	$usernameType = "email";
} else { // Yeah, else-if could've been used but was avoided for readability
	// Only if no email address is detected do we attempt a username check
	if(ctype_alnum(str_replace(array('-', '_'), '', $username))) { 
		$usernameType = "ign";
	} else {
		header("Location: /login/?error=2");
		die("Invalid username.");
	}
}

// Input validation complete.
// Now perform database checks.

try {
	$conn = new PDO("mysql:host=localhost;dbname=" . MYSQL_DATABASE, MYSQL_USERNAME, MYSQL_PASSWORD);
	$userCheck = "";
	if ($usernameType == "email") {
		$userCheck = $conn->prepare('SELECT confirmation, username FROM userdata WHERE email = ? and password = ?');
	} else {
		$userCheck = $conn->prepare('SELECT confirmation FROM userdata WHERE username = ? and password = ?');
	}
	$userCheck->bindParam(1, $username);
	$userCheck->bindParam(2, $hashed_password);
	$userCheck->execute();
	if ($userCheck->rowCount() == 0) {
		header("Location: /login/?error=3");
		die("Credentials were invalid.");
	}
	$rowData = $userCheck->fetch();
	$confirmation = $rowData["confirmation"];
	if (empty($confirmation)) {
		session_start();
		$_SESSION["user"] = $username;
		if ($usernameType == "email") {
			$_SESSION["user"] = $rowData["username"];	
		}
		header("Location: /home");
		die("Log in successful.");
	} else {
		header("Location: /login/?error=4");
		die("Account not confirmed.");
	}
} catch (Exception $ex) {
	header('Location: /login/?error=5');
	die("Server error.");
}
?>
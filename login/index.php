<?php 
	require '../config.php';
	require '../header.php'; 
	$error = $_GET["error"];
	$error_array = array("One or more fields are empty.", "Invalid username.", "Invalid credentials.", "Account not confirmed. Please check your email.", "Server error.");
	if (is_numeric($error) && $error <= 7) $msg = '<div class="alert alert-danger">' . $error_array[$error - 1] . '</div>';
?>
<body>
	<br />
	<br />
	<div class="jumbotron">
		<div class="container">
			<h1>Sign in <span style="font-size: 12px">Not registered yet? <a href="/register">Sign up</a>.</span></h1>
			<p>Please enter your username and password. Your username may either be your in-game username or email address.</p>
			<br />
			<?php if (isset($msg)) echo $msg; ?>
			<form action="login.php" method="POST">
				<label for="username">In-game username or email address</label>
				<input type="text" name="username" class="form-control" placeholder="IGN or email address..." />
				<br />
				<label for="password">Password</label>
				<input type="password" name="password" class="form-control" placeholder="Password..." />
				<br />
				<span style="float: right">
					<input type="submit" class="btn btn-success" value="Sign in"/>
				</span>
			</form>
		</div>
	</div>
</body>

# MCR
### Open-source Minecraft server banking solution

This serves to add an online banking system for both server users and staff.
It can be implemented by any user, even if you're just a player. All you need
is a stable server economy.

The administration system is currently not ready, and will be added in a further release.

Version: 0.01 alpha

TO-DO:
- Administration panel
- Password reset
- Plugin for server owners

### Installation instructions

Create a database and a database user. You will need to restore the file "restore_this.sql" and 
you will have the basic table structure.

Next, you need grant the user sufficient permissions. Generally, SELECT, UPDATE, INSERT and DELETE on the three tables is enough.
Do not assign grant permissions under any circumstances.

As stated, the administration panel is not ready. As such, adding vouchers is still done manually through
database edits.

Other requirements include a transactional email service and a server running at least PHP 7.
Make sure to configure these settings in "config.php."

Lastly, set up a daily cron job on "cron.php". It should be run at midnight, and will add interest for every account.

### Developer notes

Yes, I know it isn't running on your favourite framework. I was planning on writing this in Laravel, but decided against this to get a prototype ready faster.
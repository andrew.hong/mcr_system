<?php require 'config.php'; require 'header.php'; ?>
<body>
	<br />
	<br />
	<div class="jumbotron">
		<div class="container">
			<h1>Welcome to MCR, the all-in-one MC management system.</h1>
			<p>Thank you for using our system. To proceed, please select one of the following options.</p>
			<br />
			<span style="float: right">
				<a href="register/" class="btn btn-info">Register</a>
				<a href="login/" class="btn btn-primary">Sign in</a>
			</span>
		</div>
	</div>
</body>

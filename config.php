<?php
	// MySQL details:
	DEFINE("PAGE_TITLE", "MCR System");
	DEFINE("MYSQL_USERNAME", ""); // The database user MUST have insert, select, and basic query permissions.
	DEFINE("MYSQL_PASSWORD", "");
	DEFINE("MYSQL_DATABASE", ""); 

	// General Settings
	DEFINE("GENERAL_URL", "https://base_url.com");
	DEFINE("SYSTEM_EMAIL", "admin@example.com"); // Withdrawal requests go through this
	DEFINE("INTEREST_RATE_PER_ANNUM", "0.75"); // Interest rates offered

	// Mail settings
	DEFINE("SMTP_HOST", "smtp.server.com");
	DEFINE("SMTP_USERNAME", "smtp_username");
	DEFINE("SMTP_PASSWORD", "smtp_password");
	DEFINE("SMTP_FROM", "from_email@server.com"); // Email to send from (system notifs)
?>

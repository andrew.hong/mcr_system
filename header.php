<?php session_start(); require 'vendor/autoload.php'; ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo PAGE_TITLE; ?></title>
		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
		<style type="text/css">
			body {
				background: url("/res/bg.jpg") no-repeat center center fixed;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				-o-background-size: cover;
				background-size: cover;
			}
			.jumbotron {
				border-radius: 0px;
			}
			label {
				font-weight: bold;
			}
		</style>
		<?php if (!empty($_SESSION["user"])) { ?>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="#">MCR System</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<a class="nav-link" href="/home">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/home/transactions">Transactions</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/home/account">Account Management</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/home/send">Send money</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/home/withdraw">Withdraw</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/home/redeem">Redeem voucher</a>
					</li>
				</ul>
				<span class="navbar-text">
					Account balance: $<?php echo number_format($_SESSION["balance"], 2, '.', ''); ?> &nbsp;&nbsp;
				</span>
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link" href="/home/logout">Sign out</a>
					</li>
				</ul>
			</div>
		</nav>
		<?php } ?>
	</head>
